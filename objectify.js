'use strict';


(function () {

    /**
     * Extend an object with another
     * @method extend
     * @returns out
     */
    function extend() {
        /**
         * Test for Plain old javascript object
         * @method pojo
         * @param {} obj
         * @returns LogicalExpression
         */
        var pojo = function (obj) {
            if (!obj || Object.prototype.toString.call(obj) !== '[object Object]') return false;
            if (obj.constructor && !Object.prototype.hasOwnProperty.call(obj, 'constructor') && !( obj.constructor && obj.constructor.prototype && Object.prototype.hasOwnProperty.call(obj.constructor.prototype, 'isPrototypeOf') )) return false;
            var key = Object.keys(obj)
                .pop();
            return typeof key === 'undefined' || Object.prototype.hasOwnProperty.call(obj, key);
        };

        /** setup */
        var src, key, dst;
        /** @type {array} Arguments as an array */
        var args = Array.prototype.slice.call(arguments, 0);
        /** @type {mixed} output variable */
        var out = args[0];
        /** @type {Boolean} Deep copy */
        var deep = false;

        if (typeof out === 'boolean') {
            deep = args.shift();
            out = args[0] || {};
        } else if (( typeof out !== 'object' && typeof out !== 'function' ) || out == null) out = {};

        for (var i = 1; i < args.length; ++i) {
            src = args[i];
            if (src != null) {
                for (key in src) {
                    dst = out[key];
                    if (out !== src[key]) out[key] = ( deep && src[key] && ( pojo(src[key]) || Array.isArray(src[key]) ) ) ? ( extend(deep, ( Array.isArray(src[key]) ? ( dst && Array.isArray(dst) ? dst : [] ) : ( dst && pojo(dst) ? dst : {} ) ), src[key]) ) : ( src[key] );
                }
            }
        }

        return out;
    };


    /**
     * Description
     * @method objectify
     * @param {} m_obj
     * @returns CallExpression
     */
    var objectify = function (m_obj) {
        var plumbing = new function () {
            var plumbing = this;
            /**
             * Ensure the passed object is really an object
             * @method ensureObject
             * @param {} m_obj
             * @returns obj
             */
            this.ensureObject = function (m_obj) {
                var obj = {};
                switch (typeof( m_obj )) {
                    case 'string':
                        obj = JSON.parse(m_obj);
                        return obj;
                        break;
                    case 'object':
                        if (Array.isArray(m_obj)) {
                            // extend( obj, m_obj );
                            obj = m_obj;
                        } else {
                            obj = m_obj;
                        }
                        break;
                }
                return obj;
            };

            /**
             * resolve a namespace for a value
             * @method resolveNamespace
             * @param {} ns
             * @param {} obj
             * @param {} force
             * @returns {void}
             */
            this.resolveNamespace = function (ns, obj, force) {
                force = !!force;
                if (!ns) return obj;
                if (ns.indexOf('.') >= 0) {
                    var nsa = ns.split('.');
                    var tns = nsa.shift();
                    ns = nsa.join('.');
                    if (obj.hasOwnProperty(tns)) {
                        return this.resolveNamespace(ns, obj[tns], force);
                    } else {
                        if (force) {
                            obj[tns] = {};
                            return this.resolveNamespace(ns, obj[tns], force);
                        }
                        return;
                    }
                } else {
                    if (obj.hasOwnProperty(ns)) {
                        return obj[ns];
                    } else if (force) {
                        obj[ns] = {};
                        return obj[ns];
                    }
                    return;
                }
            };

            /**
             * enumerate all namespaces
             * @method enumerateNamespace
             * @param {} obj
             * @param {} prefix
             * @returns ret
             */
            this.enumerateNamespace = function (obj, prefix) {
                var ret = [];
                Object.keys(obj)
                    .forEach(function (v) {
                        var cpfix = ( prefix ? ( prefix + '.' ) : '' ) + v;
                        ret.push(cpfix);
                        if (typeof obj[v] == 'object') {
                            plumbing.enumerateNamespace(obj[v], cpfix)
                                .forEach(function (v) {
                                    ret.push(v);
                                });
                        }
                    });
                return ret;
            };

            /**
             * Set value at namespace
             * @method setNamespace
             * @param {} ns
             * @param {} obj
             * @param {} val
             * @returns {void}
             */
            this.setNamespace = function (ns, obj, val) {
                if (ns.indexOf('.') >= 0) {
                    var nsa = ns.split('.');
                    var tns = nsa.pop();
                    ns = nsa.join('.');
                    var tobj = this.resolveNamespace(ns, obj, true);
                    if ('object' != typeof tobj) {
                        this.setNamespace(ns, obj, {});
                        tobj = this.resolveNamespace(ns, obj, true);
                    }
                    tobj[tns] = val;
                } else {
                    if (ns) {
                        if ('object' != typeof( obj )) throw 1;
                        obj[ns] = val;
                    } else {
                        obj = val;
                    }
                }
            };

            /**
             * Remove namespace
             * @method rmNamespace
             * @param {} ns
             * @param {} obj
             * @returns {void}
             */
            this.rmNamespace = function (ns, obj) {
                if (ns.indexOf('.') >= 0) {
                    var nsa = ns.split('.');
                    var tns = nsa.pop();
                    ns = nsa.join('.');
                    var tobj = this.resolveNamespace(ns, obj);
                    if (typeof( tobj ) == 'object') delete( tobj[tns] );
                } else {
                    if (typeof( obj ) == 'object') delete obj[ns];
                }
            };

        };

        return (function (obj) {
            obj = plumbing.ensureObject(obj);
            return new function () {
                var self = this;
                var subscribers = [];

                /**
                 * get the value of the object at the indicated namespace
                 * @method get
                 * @param {} ns
                 * @param {} def
                 * @returns LogicalExpression
                 */
                this.get = function (ns, def) {
                    if (!ns) return obj;
                    return plumbing.resolveNamespace(ns, obj) || def;
                };

                /**
                 * Clone at the indicated namespace
                 * @method clone
                 * @param {} ns
                 * @returns CallExpression
                 */
                this.clone = function (ns) {
                    return objectify(self.get(ns));
                };

                /**
                 * Make a copy at the indicated namespace
                 * @method copy
                 * @param {} ns
                 * @returns CallExpression
                 */
                this.copy = function (ns) {
                    return objectify(extend(true, {}, self.get(ns)));
                };


                /**
                 * Set a namespace value
                 * @method set
                 * @param {} ns
                 * @param {} val
                 * @returns ThisExpression
                 */
                this.set = function (ns, val) {
                    if (!ns) obj = val;
                    plumbing.setNamespace(ns, obj, val);
                    subscribers.forEach(function (subscriber) {
                        subscriber(ns);
                    });
                    return this;
                };

                /**
                 * Subscribe to changes on a namespace
                 * @method subscribe
                 * @param {} cb
                 * @returns {void}
                 */
                this.subscribe = function (cb) {
                    if (typeof(cb) == 'function') subscribers.push(cb);
                };

                /**
                 * Unset a namespace
                 * @method unset
                 * @param {} ns
                 * @returns CallExpression
                 */
                this.unset = function (ns) {
                    if (!ns) obj = val;
                    return plumbing.rmNamespace(ns, obj);
                };

                /**
                 * Alias for get
                 * @method val
                 * @returns {void}
                 */
                this.val = function () {
                    switch (arguments.length) {
                        case 0:
                            return obj;
                        case 1:
                            return this.get(arguments[0]);
                        case 2:
                            return this.set(arguments[0], arguments[1]);
                    }
                };


                this.update =
                /**
                 * Apply additional object to current object
                 * @method apply
                 * @param {} ns
                 * @param {} m_obj
                 * @returns self
                 */
                    this.apply = function (ns, m_obj) {
                        var undef;
                        if (arguments.length == 1) {
                            m_obj = ns;
                            ns = undef;
                        }
                        if ('object' != typeof m_obj) throw new Error('objectify.apply requires an object to apply');

                        var applyObj = objectify({})
                            .set(( 'string' == typeof ns ? ns : '' ), plumbing.ensureObject(m_obj));
                        var tobj = objectify(plumbing.resolveNamespace(ns, obj, true));
                        applyObj.fullKeys.leaves()
                            .forEach(function (key) {
                                self.set(key, applyObj.get(key));
                            });

                        return self;
                    };

                /**
                 * Interpolate {namespace.resource.locators} in the provided string
                 * @method interpolate
                 * @param {} ns
                 * @param {} str
                 * @returns str
                 */
                this.interpolate = function (ns, str) {
                    var undef;

                    if (arguments.length == 0)return;
                    if (arguments.length == 1) {
                        str = ns;
                        ns = undef;
                    }

                    if (ns) {
                        return self.clone(ns).interpolate(str);
                    } else {
                        self.fullKeys().forEach(function (key) {
                            str = str.replace("{" + key + "}", self.get(key));
                        });
                    }
                    return str;
                };

                /**
                 * Convert to json
                 * @method json
                 * @param {} ns
                 * @returns CallExpression
                 */
                this.json = function (ns) {
                    return JSON.stringify(this.get(ns));
                };

                /**
                 * All the keys at a the indicated namespace
                 * @method keys
                 * @param {} ns
                 * @returns CallExpression
                 */
                this.keys = function (ns) {
                    return Object.keys(this.get(ns));
                };

                /**
                 * Return all namespaces
                 * @method fullKeys
                 * @param {} ns
                 * @param {} filter
                 * @returns keys
                 */
                this.fullKeys = function (ns, filter) {
                    var undefined;
                    if (arguments.length == 1 && ns instanceof RegExp) {
                        filter = ns;
                        ns = undefined;
                    }
                    var tobj = this.get(ns);
                    var keys = plumbing.enumerateNamespace(tobj);
                    if (filter) {
                        var fkeys = [];
                        var r = RegExp(filter);
                        keys.forEach(function (v) {
                            if (r.test(v)) fkeys.push(v)
                        });
                        return fkeys;
                    }
                    return keys;
                };

                /**
                 * Return all namespaces sorted by length
                 * @method length_sorted
                 * @param {} ns
                 * @param {} filter
                 * @returns CallExpression
                 */
                this.fullKeys.length_sorted = function (ns, filter) {
                    return self.fullKeys(ns, filter)
                        .sort(function (a, b) {
                            return a.length - b.length
                        });
                    ;
                };

                /**
                 * Return all namespaces sorted by depth.
                 * @method ns_sorted
                 * @param {} ns
                 * @param {} filter
                 * @returns CallExpression
                 */
                this.fullKeys.ns_sorted = function (ns, filter) {
                    return self.fullKeys(ns, filter)
                        .sort(function (a, b) {
                            return a.split('.')
                                    .length - b.split('.')
                                    .length
                        });
                    ;
                };

                /**
                 * Return all namespaces sorted by length, reversed
                 * @method reverse
                 * @param {} ns
                 * @param {} filter
                 * @returns keys
                 */
                this.fullKeys.length_sorted.reverse = function (ns, filter) {
                    var keys = self.fullKeys.length_sorted(ns, filter);
                    keys.reverse();
                    return keys;
                };

                /**
                 * Return all the namespace leaves
                 * @method leaves
                 * @param {} ns
                 * @param {} filter
                 * @returns keys
                 */
                this.fullKeys.leaves = function (ns, filter) {
                    var keys = [];
                    var fullKeys = self.fullKeys.ns_sorted(ns, filter);
                    fullKeys.forEach(function (outer, i) {
                        var found = false;
                        for (var idx = i + 1; idx < fullKeys.length; idx++) {
                            if (fullKeys[idx].indexOf(outer) >= 0) found = true;
                        }
                        if (!found) keys.push(outer);
                    });
                    return keys;
                };

                /**
                 * Pluck the values for the namespaces matching the regex filter
                 * @method pluck
                 * @param {} ns
                 * @param RegExp filter
                 * @returns CallExpression
                 */
                this.pluck = function (ns, filter) {
                    var undefined;
                    if (arguments.length == 1 && ns instanceof RegExp) {
                        filter = ns;
                        ns = undefined;
                    }

                    return this.fullKeys(ns, filter)
                        .map(function (v) {
                            return self.clone(ns)
                                .get(v);
                        });
                };

                /**
                 * Apply a filter to the object
                 * @method filter
                 * @param {} fn
                 * @returns CallExpression
                 */
                this.filter = function (fn) {
                    var ret = Array.isArray(obj) ? [] : {};
                    for (var idx in obj) {
                        if (fn(obj[idx], idx, obj)) ret[idx] = obj[idx];
                    }
                    return objectify(ret);
                };

                /**
                 * Test a value for undefined
                 * @method has
                 * @param {} ns
                 * @returns BinaryExpression
                 */
                this.has = function (ns) {
                    var v = this.get(ns);
                    return typeof v != 'undefined';
                };

                /**
                 * Provide an array of Namespaces and get an object containing only those keys
                 * @method getObject
                 * @param {} nsArray
                 * @param {} defaultIfNotFound
                 * @returns {void}
                 */
                this.getObject = function (nsArray, defaultIfNotFound) {
                    var doDefaults = typeof(defaultIfNotFound) != 'undefined';
                    if (Array.isArray(nsArray)) {
                        var nObj = objectify({});
                        for (var idx = 0; idx < nsArray.length; idx++) {
                            var ns = nsArray[idx];
                            if (this.has(ns)) {
                                nObj.set(ns, this.get(ns));
                            } else {
                                if (doDefaults) {
                                    nObj.set(ns, defaultIfNotFound);
                                }
                            }
                        }
                        return nObj;
                    } else {
                        return {};
                    }
                };

                /**
                 * Ensure a object structure is represented on the object
                 * @method ensure
                 * @param {} obj
                 * @returns nObj
                 */
                this.ensure = function (obj) {
                    var orig = this;
                    var nObj = orig.clone();
                    var eObj = objectify(obj);
                    var ensureKeys = eObj.fullKeys.leaves();

                    ensureKeys.forEach(function (ns) {
                        nObj.set(ns, orig.has(ns) ? orig.get(ns) : eObj.get(ns));
                    });

                    return nObj;
                };

                this.applyTemplate = this.ensure;
            };
        })(m_obj);
    };

    if ('undefined' != typeof module && 'exports' in module) {
        module.exports = objectify;
    } else if ('undefined' != typeof global) {
        global.objectify = objectify;
    } else if ('undefined' != typeof window) {
        window.objectify = objectify;
    }
})();