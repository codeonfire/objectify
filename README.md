# README #
### What is this repository for? ###

* NPM module to help with deeply nested object management and manipulation
* Version 1.1.0

### Installation ###
    
```
#!bash

npm install --save mh_objectify
#OR
npm install mh_objectify
```